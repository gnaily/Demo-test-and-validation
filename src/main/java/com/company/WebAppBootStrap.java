package com.company;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jooq.JooqAutoConfiguration;

/**
 * 
 * @description  entry of the web application
 */
//@SpringBootApplication equals the three: 
//@Configuration and @EnableAutoConfiguration
//and @ComponentScan
@SpringBootApplication(exclude= {JooqAutoConfiguration.class})
public class WebAppBootStrap {

	public static void main(String[] args) {
		SpringApplication.run(WebAppBootStrap.class);
	}
	
}
