package com.company.common;

import java.util.List;
/**
 * 
 * 分页结果类
 * @author company  yladdr@163.com
 * @date  2018年10月2日下午11:49:20
 *
 * @param <T>
 */
public class PageResult<T>{
	/**总记录数*/
	private long totalCount;

	/**当前页数据*/
	private List<T> curPageData;
	
	/*--------------------构造方法-----------------------------*/
	public PageResult(){}
	
	public PageResult(long totalCount,List<T> curPageData){
		this.totalCount=totalCount;
		this.curPageData=curPageData;
	}

	
	/*-----------------------getter and setter----------------*/
	/**
	 * @return 获取总记录数
	 */
	public long getTotalCount() {
		return totalCount;
	}
	/**
	 * @param totalCount 设置总记录数
	 */
	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}
	/**
	 * @return  获取当前页数据
	 */
	public List<T> getCurPageData() {
		return curPageData;
	}
	/**
	 * @param curPageData 设置当前页数据
	 */
	public void setCurPageData(List<T> curPageData) {
		this.curPageData = curPageData;
	}
	
}