package com.company.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * 
 *
 * @author company  yladdr@163.com
 * @date  2018年10月2日下午10:44:25
 *
 * @param <Type>
 */
public class Return<Type> implements Serializable{

	private static final long serialVersionUID = -9218159563573284981L;
	/**状态码*/
	private int code;
	/**状态描述*/
	private String msg;
	/**具体返回值*/
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Type result;  
	
	public Return (int code,String msg){
		this(code,msg,null);
	}
	
	public Return (int code,String msg,Type result){
		this.code=code;
		this.msg=msg;
		this.result=result;
	}
	
	public  int  getCode() {
		return code;
	}
	
	public String getMsg() {
		return msg;
	}
	
	
	public Type getResult() {
		return result;
	}
	
}
 