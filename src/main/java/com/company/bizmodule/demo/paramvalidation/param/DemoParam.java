package com.company.bizmodule.demo.paramvalidation.param;


import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.Date;

public class DemoParam {


	@Size(min=2, max =20,message = "姓名长度必须在13-19之间")
	private String name;


	//用作任意类型	验证注解的元素值不是null
	@NotNull(message = "身份证号不能为空")
	private String idNumber;


	@Future
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm",iso = DateTimeFormat.ISO.DATE_TIME)
	private Date scheduledTime;


	@Min(value=1,message = "预约窗口号最小只能为1")
	@Max(value =10,message = "预约窗口号最大只能为10")
	Integer scheduledWindow;


	@DecimalMin(value="10000")
	@DecimalMax(value="100000")
	@NumberFormat(style = NumberFormat.Style.CURRENCY)
	BigDecimal scheduledWithDraw;


	//验证注解的元素值与指定的正则表达式匹配,任何CharSequence的子类型
	@Pattern(regexp="(^[a-z0-9A-Z]+)([-a-z0-9A-Z_]+)(@)([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-z]+$",message = "邮箱格式不正确")
	String email;


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public Date getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(Date scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public Integer getScheduledWindow() {
		return scheduledWindow;
	}

	public void setScheduledWindow(Integer scheduledWindow) {
		this.scheduledWindow = scheduledWindow;
	}

	public BigDecimal getScheduledWithDraw() {
		return scheduledWithDraw;
	}

	public void setScheduledWithDraw(BigDecimal scheduledWithDraw) {
		this.scheduledWithDraw = scheduledWithDraw;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
