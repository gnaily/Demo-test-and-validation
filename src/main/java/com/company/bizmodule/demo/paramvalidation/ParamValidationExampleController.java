package com.company.bizmodule.demo.paramvalidation;

import com.company.bizmodule.demo.paramvalidation.param.DemoParam;
import com.company.common.Return;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.math.BigDecimal;

@Controller
@Validated
public class ParamValidationExampleController {


	@GetMapping("/demo/param-validation/1")
	@ResponseBody
	Return<String> one( @Size(min=2,max = 4) String string,
	                    @Pattern(regexp="^a(h)*b$") String patternString,
	                    @Min(10) @Max(100)   Integer intNumber,
	                    @DecimalMin("10.24") @DecimalMax("20.48")   BigDecimal currencyNumber ){
		return new Return<>(200,"success!");
	}


	@PostMapping("/demo/param-validation/2")
	@ResponseBody
	Return<String> two(@Valid DemoParam demoParam) {

		return new Return<>(200,"success!");
	}

}
