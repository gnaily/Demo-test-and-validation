package com.company.config;

import com.company.WebAppBootStrap;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 *
 * @description  This is only needed when we need to build a war file and deploy it
 * @author  yangliang
 *
 */
public class WebAppServletInitializer extends SpringBootServletInitializer {


	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WebAppBootStrap.class);
	}

	
}
