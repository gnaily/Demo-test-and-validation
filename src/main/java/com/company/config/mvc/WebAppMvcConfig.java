package com.company.config.mvc;

import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.format.number.CurrencyStyleFormatter;
import org.springframework.format.number.NumberStyleFormatter;
import org.springframework.format.number.PercentStyleFormatter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

@Configuration
public class WebAppMvcConfig implements WebMvcConfigurer{

	/**
	 * Cross-origin resource sharing urls config
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		//Enable cross-origin request handling for the specified path pattern
		registry.addMapping("/api/**");
	}
	
	
	
	@Override
	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters=customHttpConverters().getConverters();
	}
	
	
	/**
	 * HttpMessageConverters for converting HTTP requests and responses config
	 * @return HttpMessageConverters
	 */
	@Bean
	public HttpMessageConverters customHttpConverters() {
		
		return new HttpMessageConverters(
				  new ByteArrayHttpMessageConverter(),
				  new StringHttpMessageConverter(),
				  new CustomJsonHttpMessageConverter());
	}


	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
		
		
	}
	

	/*
	 * asynchronous request handling config
	 * 
	 */
	@Override
	public void configureAsyncSupport(AsyncSupportConfigurer configure) {
		configure.setDefaultTimeout(30*1000L);
		configure.setTaskExecutor(mvcTaskExecutor());
	}


	@Bean
    public ThreadPoolTaskExecutor mvcTaskExecutor(){
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(10);
        executor.setQueueCapacity(100);
        executor.setMaxPoolSize(25);
        return executor;
    }
	
	
	/**
	 * Interceptors config
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry){
      
        registry.addInterceptor(new LocaleChangeInterceptor());
    }


	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.favorPathExtension(true).favorParameter(false)
        .parameterName("mediaType").ignoreAcceptHeader(false)
        .defaultContentType(MediaType.APPLICATION_JSON_UTF8)
        .mediaType("xml", MediaType.APPLICATION_XML)
        .mediaType("json", MediaType.APPLICATION_JSON);
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
		//Add  Formatter to format fields of a specific type
		registry.addFormatter(new NumberStyleFormatter());
		registry.addFormatter(new PercentStyleFormatter());
		registry.addFormatter(new CurrencyStyleFormatter());
		registry.addFormatter(new DateFormatter("yyyy-MM"));
		registry.addFormatter(new StringTrimFormatter());
	}

	private class  StringTrimFormatter implements Formatter<String> {
		@Override
		public String parse(String text, Locale locale) throws ParseException {
			if(Objects.isNull(text)){
				return  null;
			}else {
				return text.trim();
			}
		}

		@Override
		public String print(String object, Locale locale) {
			return object;
		}
	}

    //------------------------------filter 配置	
//	@Bean
//	public FilterRegistrationBean<?> requestUrlFilter() {
//
//		FilterRegistrationBean<RequestUrlFilter> frb=new FilterRegistrationBean<>();
//		frb.setFilter(new RequestUrlFilter());
//		frb.addUrlPatterns("/*");
//		frb.setOrder(2);
//		return frb;
//	}
//	
//	@Bean
//	public FilterRegistrationBean<?> requestLoggingFilter() {
//
//		FilterRegistrationBean<RequestLoggingFilter> frb=new FilterRegistrationBean<>();
//		frb.setFilter(new RequestLoggingFilter());
//		frb.addUrlPatterns("/*");
//		frb.setOrder(1);
//		return frb;
//	}

}
