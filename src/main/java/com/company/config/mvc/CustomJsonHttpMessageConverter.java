package com.company.config.mvc;

import com.dslplatform.json.DslJson;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
/**
 * 
 *  自定义Json转换器，使用DslJson--High performance JVM JSON library
 * 	  来进行序列化和反序列化
 * @author yangliang
 *
 */
public class CustomJsonHttpMessageConverter extends AbstractHttpMessageConverter<Object> {
	
	public static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
	
	// DslJson 
	private final DslJson<Object> dslJson = new DslJson<>();


	public CustomJsonHttpMessageConverter(){
		setDefaultCharset(DEFAULT_CHARSET);
		// 该转换器的支持类型：application/json
		setSupportedMediaTypes( Arrays.asList(MediaType.APPLICATION_JSON));
	}
	
	@Override
	protected Object readInternal(Class<? extends Object> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		
			return dslJson.deserialize(clazz, inputMessage.getBody());
	}

	@Override
	protected void writeInternal(Object t, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {
		
			dslJson.serialize(t, outputMessage.getBody());
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		return dslJson.canDeserialize(clazz);
	}
}