package com.company.config.cache;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfig extends CachingConfigurerSupport{
	
	@Override
	public CacheManager cacheManager() {
		CaffeineCacheManager cm=new CaffeineCacheManager();
		cm.setAllowNullValues(false);
		return cm;
	}
	
}
