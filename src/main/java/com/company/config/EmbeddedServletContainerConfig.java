package com.company.config;

import org.springframework.boot.web.server.MimeMappings;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmbeddedServletContainerConfig  
	implements WebServerFactoryCustomizer<ConfigurableServletWebServerFactory>  {
	

	@Override
	public void customize(ConfigurableServletWebServerFactory server) {
	    server.setPort(9000);
		MimeMappings mappings = new MimeMappings(MimeMappings.DEFAULT);
        // IE issue, see https://github.com/jhipster/generator-jhipster/pull/711
        mappings.add("html", "text/html;charset=utf-8");
        // CloudFoundry issue, see https://github.com/cloudfoundry/gorouter/issues/64
        mappings.add("json", "text/html;charset=utf-8");
        server.setMimeMappings(mappings);
	}


	
}

