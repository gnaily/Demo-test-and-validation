package com.company.config.db;

import bitronix.tm.BitronixTransactionManager;
import org.jooq.TransactionContext;
import org.jooq.TransactionProvider;
import org.jooq.exception.DataAccessException;
import org.jooq.tools.JooqLogger;

import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;

/**
 *
 *  分布式事务配置
 *  @author yanglinag
 *
 */
public class JooqTransactionProvider implements TransactionProvider {

	private static final JooqLogger log = JooqLogger.getLogger(JooqTransactionProvider.class);

	private  final BitronixTransactionManager txMgr;

	public  JooqTransactionProvider(BitronixTransactionManager txMgr){
		this.txMgr=txMgr;
	}


	@Override
	public void begin(TransactionContext ctx) {
		try {
			txMgr.begin();
			log.debug("transaction begin");
		} catch (NotSupportedException e) {
			throw new DataAccessException("begin transaction failed",e);
		} catch (SystemException e) {
			throw new DataAccessException("begin transaction failed",e);
		}
	}

	@Override
	public void commit(TransactionContext ctx) {
		try {
			txMgr.commit();
			log.debug("transaction commited");
		} catch (Exception e) {
			throw new DataAccessException("commit transaction failed",e);
		}
	}

	@Override
	public void rollback(TransactionContext ctx) {
		try {
			txMgr.rollback();
			log.debug("transaction rollbacked ");
		} catch (Exception e) {
			throw new DataAccessException("commit transaction failed",e);
		}
	}
}

