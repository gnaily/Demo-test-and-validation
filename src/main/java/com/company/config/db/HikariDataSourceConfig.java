package com.company.config.db;

import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
/**
 *  This class config a Hikari datasource
 * @author yanglinag
 */
@Configuration
public class HikariDataSourceConfig {
	private final Logger log=LoggerFactory.getLogger(HikariDataSourceConfig.class);
	@Value("${datasource.hikari.pool-name}")
	private String poolName;
	
	@Value("${datasource.hikari.jdbc-url}")
	private String jdbcUrl;
	
	@Value("${datasource.hikari.driver-class-name}")
	private String driverClassName;
	
	@Value("${datasource.hikari.username}")
	private String username;
	
	@Value("${datasource.hikari.password}")
	private String password;
	
	@Value("${datasource.hikari.min-idle}")
	private int minIdle;
	
	@Value("${datasource.hikari.max-pool-size}")
	private int maxPoolSize;

	@Value("${datasource.hikari.max-lifetime}")
	private long maxLifetimeMs;
	
	@Value("${datasource.hikari.connection-timeout}")
	private long connectionTimeoutMs;
	
	@Value("${datasource.hikari.auto-commit}")
	private boolean isAutoCommit;
	
	@Value("${datasource.hikari.connection-test-query}")
	private String connectionTestQuery;
	
	@Value("${datasource.hikari.idle-timeout}")
	private long idleTimeout;
	
	
	@Bean(name="dataSource")
	public DataSource dataSource() {
		HikariDataSource hds=new HikariDataSource();
		hds.setPoolName(poolName);
		hds.setJdbcUrl(jdbcUrl);
		hds.setDriverClassName(driverClassName);
		hds.setUsername(username);
		hds.setPassword(password);
		hds.setMinimumIdle(minIdle);
		hds.setIdleTimeout(idleTimeout);
		hds.setMaximumPoolSize(maxPoolSize);
		hds.setAutoCommit(isAutoCommit);
		hds.setMaxLifetime(maxLifetimeMs);
		hds.setConnectionTimeout(connectionTimeoutMs);
		hds.setConnectionTestQuery(connectionTestQuery);
		log.info("HikariDataSource config finish");
		return hds;
	}
	
	

}
