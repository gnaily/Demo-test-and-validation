package com.company.filter;

import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
@Component
@WebFilter(urlPatterns= {"/"})
@Order(value=1)//过滤器的执行顺序，value越小优先级越高
public class RequestUrlFilter implements Filter{
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		String url=((HttpServletRequest)request).getRequestURI();
		
		if(!isInBlackList(url)) {
			chain.doFilter(request, response);
		}
	}
	
	@Override
	public void destroy() {
		
	}

	private String[] blackList= {"/abc/"};
	private String[] whiteList= {};
	private boolean isInBlackList(String url) {
		return false;
	}
}
