package com.company.filter;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.UUID;
//@TODO log to db
@Component
@WebFilter(urlPatterns= {"/"})
@Order(value=Integer.MAX_VALUE)
public class RequestLoggingFilter implements Filter {
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, 
			FilterChain chain) throws IOException, ServletException {
		ThreadContext.put("id", UUID.randomUUID().toString());
		Instant time = Instant.now();
		StopWatch timer = new StopWatch();
		try {
			timer.start();
			System.out.println("-----log---");
			chain.doFilter(request, response);
		} finally {
			timer.stop();
			HttpServletRequest in = (HttpServletRequest) request;
			HttpServletResponse out = (HttpServletResponse) response;
			String length = out.getHeader("Content-Length");
			if (length == null || length.length() == 0)
				length = "-";
			System.out.println(
					in.getRemoteAddr() + " - - [" + time + "]" + " \"" + in.getMethod() + " " + in.getRequestURI() + " "
							+ in.getProtocol() + "\" " + out.getStatus() + " " + length + " " + timer);
		}
	}
	
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {}

	@Override
	public void destroy() {}
}
