package com.company.exception;

/**
 * BusinessException case
 */
public class BECase {
	private int code;
	private String desc;

	public BECase(int code, String desc){
		this.code=code;
		this.desc=desc;
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}
}
