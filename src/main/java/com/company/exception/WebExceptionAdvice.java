package com.company.exception;

import com.company.common.Return;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;
/**
 * 
 * This class is used for handle Exception
 *
 */
@ControllerAdvice(basePackageClasses = WebExceptionAdvice.class)
public class WebExceptionAdvice extends ResponseEntityExceptionHandler {

	//处理Get/POST请求中 使用@Valid 验证路径中请求实体校验失败后抛出的异常
	@Override
	public ResponseEntity<Object>  handleBindException(
			BindException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		logError(request,ex);

    	List<FieldError> fieldErrors=ex.getBindingResult().getFieldErrors();
    	String msg="参数绑定失败";
		if(!CollectionUtils.isEmpty(fieldErrors)) {
			 msg+="[";
		     StringBuilder msgBuilder = new StringBuilder();
		     for (FieldError fieldError : fieldErrors) {
		    	 
		         msgBuilder.append(fieldError.getField()).append(":");
		         String defaultMsg=fieldError.getDefaultMessage();
			     msgBuilder.append( defaultMsg== null ? "" : fieldError.getDefaultMessage());
		     }
		     String errorMsg = msgBuilder.toString();
		     if (errorMsg.length() > 1) {
		    	 errorMsg = errorMsg.substring(0, errorMsg.length() - 1);
		     }
		     msg+=(errorMsg+"]");
		 }
		 Return<?> ret=new Return<>(status.value(),msg);
		 return handleExceptionInternal(ex, ret, headers, status, request);
	}
	
	
	@ExceptionHandler({ConstraintViolationException.class})
	public ResponseEntity<Return<?>> handleConstraintViolationException(HttpServletRequest request, ConstraintViolationException exception) {
		
		logError(request,exception);
		Set<ConstraintViolation<?>> violations= exception.getConstraintViolations();
		
		String msg="参数验证失败";
		if(!CollectionUtils.isEmpty(violations)) {
			msg+="[";

			StringBuilder msgBuilder = new StringBuilder();
			for (ConstraintViolation<?> violation : violations) {
				 msgBuilder.append(violation.getMessage()).append(",");
			}
		    
		    String errorMsg = msgBuilder.toString();
		    if (errorMsg.length() > 1) {
		    	 errorMsg = errorMsg.substring(0, errorMsg.length() - 1);
		    }
		    msg+=(errorMsg+"]");
		 } 
		HttpStatus status=HttpStatus.BAD_REQUEST;
		Return<?> ret=new Return<>(status.value(),msg);
		return new ResponseEntity<>(ret, status);
	}

	
	
	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleMethodArgumentNotValid(org.springframework.web.bind.MethodArgumentNotValidException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		logError(request,ex);
		
		String msg="参数验证失败";
        List<ObjectError>  objectErrors = ex.getBindingResult().getAllErrors();
        if(!CollectionUtils.isEmpty(objectErrors)) {
            StringBuilder msgBuilder = new StringBuilder();
            for (ObjectError objectError : objectErrors) {
                msgBuilder.append(objectError.getDefaultMessage()).append(",");
            }
            String errorMessage = msgBuilder.toString();
            if (errorMessage.length() > 1) {
                errorMessage = errorMessage.substring(0, errorMessage.length() - 1);
            }
            msg+=(errorMessage+"]");
        }
        Return<?> ret=new Return<>(status.value(),msg);
		return handleExceptionInternal(ex, ret, headers, status, request);
	}
    
	@Override
	public  ResponseEntity<Object> handleMissingServletRequestParameter(
			  MissingServletRequestParameterException ex,
			  HttpHeaders headers, HttpStatus status, WebRequest request) {
		  
		logError(request,ex);
		  
	    return handleExceptionInternal(ex,
	    		new Return<>(status.value(),"缺少请求参数"), headers, status, request);
	}
	
	
	@Override
	public  ResponseEntity<Object>handleHttpRequestMethodNotSupported(
			HttpRequestMethodNotSupportedException ex, 
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		logError(request,ex);
		
		return handleExceptionInternal(ex, 
				new Return<>(status.value(),"不支持当前请求方法"), headers, status, request);
	}

	/* (non-Javadoc)
	 * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleHttpMediaTypeNotSupported(org.springframework.web.HttpMediaTypeNotSupportedException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
	 */
	@Override
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(HttpMediaTypeNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		logError(request,ex);
		
		return handleExceptionInternal(ex,	
				new Return<>(status.value(),"不支持当前媒体类型"), headers, status, request);
	}
	
    
    /* (non-Javadoc)
     * @see org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler#handleServletRequestBindingException(org.springframework.web.bind.ServletRequestBindingException, org.springframework.http.HttpHeaders, org.springframework.http.HttpStatus, org.springframework.web.context.request.WebRequest)
     */
    @Override
    protected ResponseEntity<Object> handleServletRequestBindingException(ServletRequestBindingException ex,
    		HttpHeaders headers, HttpStatus status, WebRequest request) {

    	logError(request,ex);
    	Return<?> ret=new Return<>(status.value(),"请求参数错误(ServletRequestBindingException)" + ex.getMessage());
		return  handleExceptionInternal(ex, ret, headers, status, request);
    }
    

	@ExceptionHandler({DataIntegrityViolationException.class})
	public ResponseEntity<Return<?>> handleIntegrityViolationException(HttpServletRequest request, 
			DataIntegrityViolationException exception) {
		
    	logError(request,exception);

    	HttpStatus status=HttpStatus.INTERNAL_SERVER_ERROR;
    	Return<?> ret=new Return<>(status.value(),"系统内部操作数据库出现异常：可能字段重复、插入冲突等");

		return new ResponseEntity<>(ret, status);
	}

	
	
	@ExceptionHandler({Exception.class})
	public ResponseEntity<Return<?>> handleException(HttpServletRequest request, Exception exception) {
		
		logError(request,exception);
		
		HttpStatus status=HttpStatus.EXPECTATION_FAILED;
		Return<?> ret =  new Return<>(status.value(),"未知错误");
		return new ResponseEntity<>(ret, status);
	}
	

	//------------------------helper function 
	//TODO
	private void logError(HttpServletRequest request,Exception exception) {
		//String message = "请求出错，请求地址和参数：xx" ;
		//logger.error(message, exception);
	}

	//TODO
	private void logError(WebRequest request,Exception exception) {
		//String message = "请求出错，请求地址和参数：xx";
		//logger.error(message, exception);
	}
}