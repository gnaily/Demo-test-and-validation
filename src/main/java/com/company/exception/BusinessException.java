package com.company.exception;

public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 2452220546812591357L;

    private BECase beCase;
	public BusinessException(BECase beCase, Throwable cause,String message) {
		super(message, cause);
		this.beCase=beCase;
	}
	
	public BusinessException(String message,Throwable cause) {
		super(message, cause);
	}
	
}
