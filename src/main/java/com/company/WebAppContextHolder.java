package com.company;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
/**
 * 
 * @description this class will hold an instance of</br>
 * Spring Container.
 *
 */
@Component
public class WebAppContextHolder implements ApplicationContextAware{
	private final Logger log=LoggerFactory.getLogger(WebAppContextHolder.class);

	private static ApplicationContext context;
	
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		WebAppContextHolder.context=context;
		log.info("initial WebAppContextHolder finish:{}",this);
	}

	
	public static ApplicationContext getContex(){
		return context;
	}
	
    @SuppressWarnings("unchecked")
	public static <T> T getBean(String beanName) {
        return (T) context.getBean(beanName);
    }

    public static <T> T getBean(Class<T> beanClass) {
        return context.getBean(beanClass);
    }
	
}
