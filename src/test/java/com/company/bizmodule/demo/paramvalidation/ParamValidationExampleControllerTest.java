package com.company.bizmodule.demo.paramvalidation;

import com.company.test.BaseApplicationTest;
import org.junit.Test;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ParamValidationExampleControllerTest extends BaseApplicationTest {

	/**
	 *{@link ParamValidationExampleController#one}
	 */
	@Test
	public void one() throws Exception {
		String url="/demo/param-validation/1";
		execute(

				new MockHttpServletRequestBuilder[]{

						get(url).param("string","")
								.param("patternString","ahhhhhhhb")
								.param("intNumber","15")
								.param("currencyNumber","20.3333"),


						get(url)
								.param("patternString","ahhhhhhh")
								.param("intNumber","102")
								.param("currencyNumber","21.2222")


				},
				(request)->{
					 mockMvc.perform(request).andDo(print())
							.andExpect(status().isOk())
							.andReturn();
				});
	}

	/**
	 *{@link ParamValidationExampleController#two}
	 */
	@Test
	public void two() {
		String url="/demo/param-validation/2";

		execute(

				new MockHttpServletRequestBuilder[]{

						post(url).param("name","张三")
								.param("idNumber","5113211887654321")
								.param("scheduledTime","2019-08-22 15:30")
								.param("scheduledWindow","5")
								.param("scheduledWithDraw","10000.3333")
								.param("email","112114119@qq.vip.com"),



						post(url).param("name","1")
								.param("idNumber"," ")
								.param("scheduledTime","2019-06-22 15:30:29")
								.param("scheduledWindow","5")
								.param("scheduledWindow","200000.035")
								.param("email","112114119@_qq.vip.com")
				},

				(request)->{
					mockMvc.perform(request).andDo(print())
							.andExpect(status().isOk())
							.andReturn();
				});
	}
}